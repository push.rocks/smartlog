/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartlog',
  version: '3.0.3',
  description: 'minimalistic distributed and extensible logging tool'
}
